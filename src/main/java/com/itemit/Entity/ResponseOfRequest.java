package com.itemit.Entity;

import com.itemit.Dao.MockDao;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* In order to use api.ai effectively, we will need to create intents on api.ai that will determine what the user wants
 * (from the sentence they typed on the search bar), and know what parameters are reauired for it.
 * Then, we will need to create a private function and case here in this ResponseOfRequest class to proceed with the
 * actual request to the database. All intents must have their associated function.
 *
 * Right now, the response is simply taking the intentName to determine the data to look for in the DB.
 * (here it is my own class MockDao, but it should not be too difficult to change it to the actual DB once we have access to it)
 * Once it has the data, it fills the formatted answer accordingly and it will be returned to the api.ai
 */


public class ResponseOfRequest {
    private String speech;
    private String displayText;
    private String data;
    private ArrayList<ContextOfRequest> contextOut;
    private String source;


    private void fillDummyParameters(){
        this.speech = "testSpeech";
        this.displayText = "testDisplay";
        this.data = "[]";
        this.contextOut = new ArrayList<ContextOfRequest>();
        this.source = "myNiceNeighbour";
    }

    // Here are a few mock requests to the database to test it
    private void nomnomz(MockDao mockDao, MockRequest mockRequest){
        /* Dummy test
         * Key words: -none-
         */
        this.speech = "I just got a Nomnomz request!";
        this.displayText = "Nomnomz request processed";
        this.data = "[]";
        this.contextOut = new ArrayList<ContextOfRequest>();
        this.source = "myself";
    }
    private void itemCountTotal(MockDao mockDao, MockRequest mockRequest){
        /* Question: "How many items have I seen?"
         * Key words: -none-
         */
        int itemCount = mockDao.itemCountTotal();
        this.speech = "I just found " + itemCount + " items for you.";
        this.displayText = itemCount + " items found!";
        this.data = "[]";
        this.contextOut = new ArrayList<ContextOfRequest>();
        this.source = "Robert I";
    }
    private void itemCountWithName(MockDao mockDao, MockRequest mockRequest){
        /* Question: "How many ItemA have I seen?"
         * Key words: itemA  -  parameter "ItemName"
         */
        String name = (String) mockRequest.getResult().getParameters().get("ItemName");
        int itemcount = mockDao.itemCountWithName(name);
        this.speech = "I just found " + itemcount + " items with this name for you.";
        this.displayText = itemcount + " items by the name \"" + name + "\"";
        this.data = "[]";
        this.contextOut = new ArrayList<ContextOfRequest>();
        this.source = "Henry VIII";
    }
    private void getItemWithSerial(MockDao mockDao, MockRequest mockRequest){
        /* Question: "Show me the items with the serial SerialA"
         * Key words: SerialA  -  parameter "ItemSerial"
         */
        String serial = (String) mockRequest.getResult().getParameters().get("ItemSerial");
        ArrayList<String> UUIDs = mockDao.getItemsWithSerial(serial);
        Iterator<String> i = UUIDs.iterator();
        this.speech = "I just found items with this serial for you.";
        this.displayText = "Items by the serial \"" + serial + "\" found!";
        this.data = "[";
        if(i.hasNext()) {
            this.data += i.next();
        }
        while (i.hasNext()){
            this.data += ", " + i.next();
        }
        this.data += "]";
        this.contextOut = new ArrayList<ContextOfRequest>();
        this.source = "wikipedia";
    }

    public ResponseOfRequest(){
        this.fillDummyParameters();
    }

    public ResponseOfRequest(MockDao mockDao, MockRequest mockRequest){
        String intentName = mockRequest.getResult().getMetadata().getIntentName();

        if(intentName.equals("Nomnomz")){
            this.nomnomz(mockDao, mockRequest);
        } else if(intentName.equals("ItemCountTotal")){
            this.itemCountTotal(mockDao, mockRequest);
        } else if(intentName.equals("ItemCountWithName")){
            this.itemCountWithName(mockDao, mockRequest);
        } else if(intentName.equals("GetItemsWithSerial")){
            this.getItemWithSerial(mockDao, mockRequest);
        }

        else {
            this.fillDummyParameters();
        }
    }

    @Override
    public String toString(){
        String speechStr = "\"speech\" : \"" + this.speech + "\"";
        String displayTextStr = "\"displayText\" : \"" + this.displayText + "\"";
        String dataStr = "\"data\" : {" + this.data + "}";
        String contextOutStr = "\"contextOut\" : " + "[";
        Iterator i = this.contextOut.iterator();
        while (i.hasNext()) {
            contextOutStr += i.next().toString();
        }
        contextOutStr += "]";
        String sourceStr = "\"source\" : \"" + source + "\"";

        return ("{" + speechStr + ", " + displayTextStr + ", " + dataStr + ", " + contextOutStr + ", " + sourceStr + "}");
    }

}
