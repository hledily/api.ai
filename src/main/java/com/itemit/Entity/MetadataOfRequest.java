package com.itemit.Entity;

import org.json.simple.JSONObject;

public class MetadataOfRequest {
    private String intentId;
    private String webhookUsed;
    private String webhookForSlotFillingUsed;
    private String intentName;

    public MetadataOfRequest(JSONObject metadataObj){
        this.intentId = (String) metadataObj.get("intentId");
        this.webhookUsed = (String) metadataObj.get("webhookUsed");
        this.webhookForSlotFillingUsed = (String) metadataObj.get("webhookForSlotFillingUsed");
        this.intentName = (String) metadataObj.get("intentName");
    }

    public String getIntentName(){
        return intentName;
    }
}
