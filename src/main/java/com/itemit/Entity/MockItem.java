package com.itemit.Entity;

public class MockItem {

    private int id;
    private String name;
    private String serial;

    public MockItem(int id, String name, String serial){
        this.id = id;
        this.name = name;
        this.serial = serial;
    }

    public MockItem(){}

    public void setId(int id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setSerial(String serial){
        this.serial = serial;
    }

    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getSerial(){
        return serial;
    }

    @Override
    public String toString(){
        String nameStr = "\"name\" : \"" + this.name + "\"";
        String serialStr = "\"serial\" : \"" + this.serial + "\"";

        return ("{" + nameStr + ", " + serialStr + "}");
    }
}
