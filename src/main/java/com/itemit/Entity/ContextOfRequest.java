package com.itemit.Entity;

import org.json.simple.JSONObject;

import java.util.Collection;

public class ContextOfRequest {
    private String name;
    private JSONObject parameters;
    private long lifespan;

    public ContextOfRequest(JSONObject contextObj) {
        if (contextObj.containsKey("name") && contextObj.containsKey("parameters") && contextObj.containsKey("lifespan")) {
            this.name = (String) contextObj.get("name");
            this.parameters = (JSONObject) contextObj.get("parameters");
            this.lifespan = (long) contextObj.get("lifespan");
        } else{
            // HANDLE ERROR LATER
        }
    }
}
