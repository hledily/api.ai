package com.itemit.Entity;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class FulfillmentOfRequest {
    private String speech;
    private ArrayList<MessageOfRequest> messages;

    public FulfillmentOfRequest(JSONObject fulfillmentObj){
        this.speech = (String) fulfillmentObj.get("speech");
        this.messages = new ArrayList<MessageOfRequest>();
        JSONArray messageArray = (JSONArray) fulfillmentObj.get("messages");
        Iterator i = messageArray.iterator();
        while(i.hasNext()){
            this.messages.add(new MessageOfRequest((JSONObject) i.next()));
        }
    }
}
