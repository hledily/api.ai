package com.itemit.Entity;

import org.json.simple.JSONObject;

public class StatusOfRequest {
    private String errorType;
    private long code;

    public StatusOfRequest(JSONObject statusObj){
        this.errorType = (String) statusObj.get("errorType");
        this.code = (long) statusObj.get("code");
    }
}
