package com.itemit.Controller;

import com.itemit.Dao.MockDao;
import com.itemit.Entity.MockItem;
import com.itemit.Entity.MockRequest;
import com.itemit.Entity.ResponseOfRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.Collection;


/* I have decided to keep this controller very simple to work with the api.ai POST queries :
 * The query is recieved in a String, parsed during the instanciation of MockRequest,
 * and processed during the instanciation of ResponseOfRequest.
 */


@RestController
@RequestMapping("/items")
public class MockItemController {

    @Autowired
    private MockDao mockDao;

    @GetMapping()
    public Collection<MockItem> getAllItems(){
        return this.mockDao.getAllItems();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public String processRequest(@RequestBody String requestStr){
        MockRequest mockRequest = new MockRequest(requestStr);
        ResponseOfRequest responseOfRequest = new ResponseOfRequest(mockDao, mockRequest);
        return responseOfRequest.toString();
    }
}
