package com.itemit.Dao;


import com.itemit.Entity.MockItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

/* This is the mock database I used for testing.
 * It has some functions to work as requests to the database.
 */



@Repository
public class MockDao {

    @Autowired
    private static HashMap<Integer, MockItem> mockItems;

    static{
        mockItems = new HashMap<Integer, MockItem>(){
            {
                put(1, new MockItem(1, "A", "text"));
                put(42, new MockItem(42, "B", "text"));
                put(53, new MockItem(53, "B", "text2"));
            }
        };
    }

    public Collection<MockItem> getAllItems(){
        return mockItems.values();
    }

    public int itemCountTotal(){
        return mockItems.size();
    }

    public int itemCountWithName(String name){
        int itemCount = 0;
        Iterator<MockItem> i = mockItems.values().iterator();
        while (i.hasNext()){
            if(i.next().getName().equals(name))
                ++itemCount;
        }
        return itemCount;
    }

    public ArrayList<String> getItemsWithSerial(String serial){
        ArrayList<String> UUIDs = new ArrayList<String>();
        Iterator<MockItem> i = mockItems.values().iterator();
        while (i.hasNext()) {
            MockItem mockItem = i.next();
            if(mockItem.getSerial().equals(serial)){
                UUIDs.add(String.valueOf(mockItem.getId()));
            }
        }
        return UUIDs;
    }

}
